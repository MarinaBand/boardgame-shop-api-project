-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 06 2022 г., 16:24
-- Версия сервера: 10.4.21-MariaDB
-- Версия PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `boardgames_shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addresses`
--

CREATE TABLE `addresses` (
  `address_id` int(10) UNSIGNED NOT NULL,
  `address_name` text NOT NULL,
  `address_user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `addresses`
--

INSERT INTO `addresses` (`address_id`, `address_name`, `address_user_id`) VALUES
(1, '191144, Санкт-Петербург, Советская 8-я, 44, кв.5', 2),
(2, '195009, Санкт-Петербург, Боткинская, 1, кв.23', 3),
(3, '194064, Санкт-Петербург, Тихорецкий проспект, 5 к2, кв.54', 1),
(4, '198262, Санкт-Петербург, Дачный проспект, 5 к3, кв.123', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(10) UNSIGNED NOT NULL,
  `cart_game_id` int(10) UNSIGNED NOT NULL,
  `cart_game_count` tinyint(2) UNSIGNED NOT NULL,
  `cart_order_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `carts`
--

INSERT INTO `carts` (`cart_id`, `cart_game_id`, `cart_game_count`, `cart_order_id`) VALUES
(1, 2, 1, 1),
(2, 5, 1, 1),
(3, 4, 1, 2),
(4, 1, 1, 2),
(5, 3, 1, 3),
(6, 6, 5, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_is_deleted` tinyint(1) UNSIGNED NOT NULL COMMENT 'Флаг для удаления'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_is_deleted`) VALUES
(1, 'Детективные игры', 0),
(3, 'Стратегические игры', 0),
(4, 'Экономические игры', 0),
(5, 'Кооперативные игры', 0),
(6, 'Для компании', 0),
(7, 'Дуэльные игры', 0),
(8, 'Для всей семьи', 0),
(9, 'Для детей', 0),
(10, 'Для взрослых', 0),
(11, 'Дополнения', 0),
(15, '111', 1),
(16, 'Игры про котиков!', 1),
(28, 'New category', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `connects`
--

CREATE TABLE `connects` (
  `connect_id` int(10) UNSIGNED NOT NULL,
  `connect_user_id` int(10) UNSIGNED NOT NULL,
  `connect_token` char(32) NOT NULL,
  `connect_token_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `connects`
--

INSERT INTO `connects` (`connect_id`, `connect_user_id`, `connect_token`, `connect_token_time`) VALUES
(9, 5, '1J2J3BTT4M4V1A0G2HK86QUPJWZALR0E', '2021-11-26 10:17:53'),
(20, 7, 'L4LU84XCVT4FEZLW6XCYKTSDFIOHAYSY', '2021-12-24 19:51:48'),
(21, 6, '467Z9XWT9LRIUOQF06UGJQKJWJ9MSP54', '2022-02-01 11:20:34'),
(22, 6, 'M5AQ2QF33C1XC95BFKJT03MI86WMSY2V', '2022-02-02 14:59:49'),
(26, 6, 'OAO7SN86H0M8B6B45M65T9OC4100JV09', '2022-02-05 16:48:30');

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `game_id` int(10) UNSIGNED NOT NULL,
  `game_name` varchar(255) NOT NULL,
  `game_price` smallint(5) UNSIGNED DEFAULT NULL,
  `game_players_count_min` tinyint(2) UNSIGNED DEFAULT NULL,
  `game_players_count_max` tinyint(2) UNSIGNED DEFAULT NULL,
  `game_age_restrictions` tinyint(2) UNSIGNED DEFAULT NULL,
  `game_weight` smallint(5) UNSIGNED DEFAULT NULL,
  `game_count` smallint(5) UNSIGNED DEFAULT NULL,
  `game_description` text DEFAULT NULL,
  `game_is_deleted` tinyint(1) UNSIGNED NOT NULL COMMENT 'Флаг для удаления',
  `game_average_mark` float(3,2) UNSIGNED DEFAULT NULL,
  `game_img_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`game_id`, `game_name`, `game_price`, `game_players_count_min`, `game_players_count_max`, `game_age_restrictions`, `game_weight`, `game_count`, `game_description`, `game_is_deleted`, `game_average_mark`, `game_img_name`) VALUES
(1, 'Шакал: Остров сокровищ', 2490, 2, 4, 8, 840, 45, 'Этот «Шакал» — всё та же замечательная стратегия родом из МГУ 70-х, ставшая бестселлером в нашей стране и лучшей стратегией 2012 года. Вы командуете отважными пиратами, которые высаживаются на остров в поисках сокровищ. Задача — найти золото и перетащить его к себе на корабль. Остров состоит из клеток, на каждой из которых что-то происходит: можно найти форт, пушку, встретить крокодила, обнаружить бутылку с посланием или откопать один из кладов капитана Шакала.', 0, 4.50, 'jackal-tresure_island.jpg'),
(2, 'Активити', 2990, 3, 16, 12, 1080, 50, 'Активити - чертовски популярная игра на объяснение слов. В 2020 году игре стукнул тридцатник, и это весьма солидный срок! Продано 10 миллионов игр, 15 вариантов, бесчисленное количество поклонников - всем нравится объяснять, рисовать и показывать слова. Всего одна коробка игры позволит вам развлечь целых 16 человек - согласитесь, удивительно полезная штука. Те, кто такими компаниями не собираются, для вас тоже есть бонус - минимальное количество участников - три. Весело будет всем!', 0, 4.67, 'activity.jpg'),
(3, 'Диксит', 1650, 3, 6, 8, 800, 25, '«Dixit» — это игра про ассоциации. У каждого игрока есть карточки с необычными картинками, которым нужно давать названия. Другие игроки пытаются отгадать эту ассоциацию.\r\nА теперь самое интересное: выиграть тут можно только тогда, когда вы сможете подобрать такую ассоциацию, которая не будет слишком сложной или слишком простой для всех игроков. Если никто не отгадывает — вы взяли слишком высоко и не получаете очков. Если отгадывают все — аналогично.', 0, 5.00, 'dixit.jpg'),
(4, 'Цитадели', 790, 2, 7, 10, 270, 45, 'Мир интриг и заговоров, благородства и дипломатии встречает вас с распростертыми объятиями! Цитадели — эталонный коктейль из психологии и стратегии. Перед вами всемирно известный настольный хит, но в исходной комплектации. \r\nКаждый ее игрок должен возвести город, превосходящий сооружения дружественных соседей, способный затмить своим величием и не менее гениальные враждебные столицы. Начиная играть, вы никогда не будете знать, откуда ожидать помощи, а кто способен воткнуть кинжал в спину и в неподходящий момент украсть ваше золото. Будьте предельно осторожны!', 0, NULL, 'citadels.jpg'),
(5, 'Кодовые имена', 1490, 2, 8, 10, 660, 75, 'Кодовые имена — это командная игра, в которой капитаны шпионских организаций помогают своим отыскать всех тайных агентов. Она необычная и достаточно простая, поэтому затягивает на несколько часов, хотя можно успеть и за тридцать минут. Всё зависит от догадливости команды и умения капитана правильно подобрать общую ассоциацию. А, кстати, среди персонажей попадаются мирные люди, двойные агенты и злобный убийца — вот его лучше ни за что не находить, отмена операции, отмена операции! Игра необычная и достаточно простая, поэтому затягивает на несколько часов. Не зря она стала игрой года в 2017.', 0, 5.00, 'codenames.jpg'),
(6, 'Сет', 890, 1, 4, 6, 300, 40, 'Для любителей игр, в которых нужно на скорость собирать разнообразные комбинации, вскрылась довольно старая, но надёжно спрятанная игра Сет (знакомое всем слово, обозначающее «набор»). Среди лаконичных, вроде разных, но подозрительно похожих карточек нужно находить правильные наборы раньше, чем противники. Это всегда весело, азартно, включает мозг, выработку адреналина и хорошее настроение. А правила вроде бы совсем простые.', 0, NULL, 'set.jpg'),
(7, 'Запретная пустыня', 1990, 2, 5, 8, 810, 47, 'Для любителей кооперативных игр Запретная пустыня — просто рай. А, нет, не рай: там же песчаная буря, ваш вертолёт разбился над раскопками древнего города, и выбраться вы сможете только на крайне ненадёжном мифическом летательном аппарате, куски которого закопаны где-то под землёй. Но у вас мало воды, и, если хоть один погибнет в процессе раскопок, вы сами станете экспонатами древнего города.', 0, NULL, 'forbidden-desert.jpg'),
(8, 'Запретная пустыня', 0, 0, 0, 0, 0, 0, '', 1, NULL, ''),
(9, 'тест', 0, 0, 0, 0, 0, 0, '', 1, NULL, ''),
(10, 'Edited test', 1500, 2, 5, 8, 810, 10, 'new new new new new new test description', 1, NULL, 'маска.jpg'),
(16, 'qwe', 0, 0, 0, 0, NULL, 0, 'qwe', 1, NULL, 'ryan-wallace-kTNS9ElVxE4-unsplash.jpg'),
(17, 'Страдающее средневековье', 1490, 3, 6, 18, NULL, 25, 'Игра от создателей одноименного коммьюнити «Вконтакте» и New Making Studio. \r\nВы возьмете на себя роль лорда, и, расселяя горожан, пытаясь спасти население от чумы и получить как можно больше славы в рестовых походах, будете вести свой славный город к величию... или смерти. ', 0, NULL, '1-1024x1024-wm.jpg'),
(19, 'Клуэдо', 2690, 2, 6, 8, NULL, 15, 'Классический детективный сюжет теперь сделает и вас героем собственного расследования. Гости собрались в старинный особняк на празднование тридцатилетия хозяина, но... О ужас, хозяин мёртв! И это всё, что известно о преступлении. Нужно выяснить, кто его убил, где это произошло и как это было сделано.', 0, NULL, '2-1024x1024-wm.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `games_categories`
--

CREATE TABLE `games_categories` (
  `game_category_id` int(10) UNSIGNED NOT NULL,
  `game_category_game_id` int(10) UNSIGNED NOT NULL,
  `game_category_category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games_categories`
--

INSERT INTO `games_categories` (`game_category_id`, `game_category_game_id`, `game_category_category_id`) VALUES
(1, 1, 3),
(2, 1, 7),
(3, 1, 8),
(4, 2, 6),
(5, 2, 8),
(6, 2, 9),
(7, 3, 6),
(8, 3, 8),
(9, 3, 10),
(10, 6, 9),
(11, 6, 8),
(12, 4, 4),
(13, 4, 6),
(14, 5, 6),
(15, 5, 8),
(16, 5, 10),
(26, 7, 5),
(27, 7, 8),
(28, 7, 10),
(30, 9, 3),
(31, 9, 5),
(42, 10, 3),
(43, 10, 5),
(44, 16, 1),
(45, 16, 4),
(46, 16, 7),
(47, 17, 10),
(50, 19, 1),
(51, 19, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `marks`
--

CREATE TABLE `marks` (
  `mark_id` int(10) UNSIGNED NOT NULL,
  `mark_value` tinyint(1) UNSIGNED NOT NULL,
  `mark_game_id` int(10) UNSIGNED NOT NULL,
  `mark_user_id` int(10) UNSIGNED NOT NULL,
  `mark_comment` text DEFAULT NULL,
  `mark_date` date NOT NULL,
  `mark_is_moderated` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `marks`
--

INSERT INTO `marks` (`mark_id`, `mark_value`, `mark_game_id`, `mark_user_id`, `mark_comment`, `mark_date`, `mark_is_moderated`) VALUES
(1, 5, 2, 3, 'Веселая игра на большую компанию. Подойдет как новичкам, так и опытным игрокам', '2022-02-01', 1),
(2, 5, 5, 3, 'Игра на ассоциации. Понятные правила, не надоедает. Хорошо подойдет для малознакомой компании', '2022-02-02', 1),
(3, 4, 2, 1, 'Игра понравилась, но больше подходит для большой компании. ', '2022-02-02', 1),
(4, 5, 2, 6, 'Отличная игра, одна из любимых. Очень много карточек, не повторяются', '2022-02-02', 1),
(5, 4, 1, 7, NULL, '2022-02-02', 1),
(6, 5, 1, 6, NULL, '2022-02-02', 1),
(7, 5, 3, 6, 'Test comment', '2022-02-03', 1),
(8, 5, 4, 6, 'Test comment', '2022-02-03', 0),
(9, 5, 5, 4, 'Отличная игра', '2022-03-01', 0),
(15, 5, 5, 2, 'Супер! играем только в нее', '2022-03-01', 0),
(16, 4, 5, 2, 'Харошая игра', '2022-03-01', 0);

--
-- Триггеры `marks`
--
DELIMITER $$
CREATE TRIGGER `update_game_average_mark_on_moderated_mark` AFTER UPDATE ON `marks` FOR EACH ROW UPDATE `games`
SET `game_average_mark` = (
    SELECT AVG(`mark_value`)
    FROM `marks`
    WHERE `mark_game_id` = NEW.`mark_game_id`
    AND `mark_is_moderated` = 1
)
WHERE `game_id` = NEW.`mark_game_id`
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_address_id` int(10) UNSIGNED DEFAULT NULL,
  `order_delivery_date` date DEFAULT NULL,
  `order_status_id` tinyint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `order_address_id`, `order_delivery_date`, `order_status_id`) VALUES
(1, 2, '2021-11-26', 4),
(2, 3, '2021-11-30', 2),
(3, 1, '2021-12-15', 1),
(4, 4, '2021-11-28', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `role_id` tinyint(2) UNSIGNED NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Администратор'),
(2, 'Модератор'),
(3, 'Пользователь');

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
  `status_id` tinyint(2) UNSIGNED NOT NULL,
  `status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'В обработке'),
(4, 'Доставлен'),
(3, 'Отправлен'),
(2, 'Сборка');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` char(32) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_phone` varchar(30) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_role_id` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_name`, `user_phone`, `user_email`, `user_dob`, `user_role_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ирина', '79111111111', 'admin@test.ru', '2001-01-01', 1),
(2, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'Иван', '79111111112', 'user1@test.ru', '1992-02-02', 3),
(3, 'user2', '7e58d63b60197ceb55a1c487989a3720', 'Олег', '79211111113', 'user2@test.ru', '1983-03-03', 3),
(4, 'user3', '92877af70a45fd6a2ed7fe81e1236b78', 'Оксана', '79041111114', 'user3@test.ru', '1994-04-04', 3),
(5, 'admin2', '3608f1747154e77c5ba347269a6c83e0', NULL, NULL, 'abc@test.ru', NULL, 1),
(6, '111', 'b59c67bf196a4758191e42f76670ceba', 'Александр', NULL, 'qwe@test.ru', NULL, 2),
(7, 'user4', 'c97ef147605469653ae4f4fef9331506', NULL, NULL, 'user4@test.ru', NULL, 3),
(10, 'moderator', 'e86fca00d794acfa2c2b6ed5e7f7501b', NULL, NULL, 'moderator@test.ru', NULL, 2),
(34, 'qwe@qwe.ru', '84b442d8bb8ba3201ebbca8eeca48154', 'New User', '+7 (912) 111-11-11', 'qwe@qwe.ru', NULL, 3),
(44, 'test@test.test', '84b442d8bb8ba3201ebbca8eeca48154', 'Иван', '+7 (912) 345-67-89', 'test@test.test', NULL, 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `address_user_id` (`address_user_id`);

--
-- Индексы таблицы `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_game_id` (`cart_game_id`),
  ADD KEY `cart_order_id` (`cart_order_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Индексы таблицы `connects`
--
ALTER TABLE `connects`
  ADD PRIMARY KEY (`connect_id`),
  ADD KEY `connect_user_id` (`connect_user_id`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`game_id`);

--
-- Индексы таблицы `games_categories`
--
ALTER TABLE `games_categories`
  ADD PRIMARY KEY (`game_category_id`),
  ADD KEY `game_category_game_id` (`game_category_game_id`),
  ADD KEY `game_category_category_id` (`game_category_category_id`);

--
-- Индексы таблицы `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`mark_id`),
  ADD KEY `mark_game_id` (`mark_game_id`),
  ADD KEY `mark_user_id` (`mark_user_id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_address_id` (`order_address_id`),
  ADD KEY `order_status_id` (`order_status_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`),
  ADD UNIQUE KEY `status_name` (`status_name`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_login` (`user_login`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD UNIQUE KEY `user_phone` (`user_phone`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addresses`
--
ALTER TABLE `addresses`
  MODIFY `address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `connects`
--
ALTER TABLE `connects`
  MODIFY `connect_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `game_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `games_categories`
--
ALTER TABLE `games_categories`
  MODIFY `game_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `marks`
--
ALTER TABLE `marks`
  MODIFY `mark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`address_user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`cart_game_id`) REFERENCES `games` (`game_id`),
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`cart_order_id`) REFERENCES `orders` (`order_id`);

--
-- Ограничения внешнего ключа таблицы `connects`
--
ALTER TABLE `connects`
  ADD CONSTRAINT `connects_ibfk_1` FOREIGN KEY (`connect_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `games_categories`
--
ALTER TABLE `games_categories`
  ADD CONSTRAINT `games_categories_ibfk_1` FOREIGN KEY (`game_category_game_id`) REFERENCES `games` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `games_categories_ibfk_2` FOREIGN KEY (`game_category_category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`mark_game_id`) REFERENCES `games` (`game_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `marks_ibfk_2` FOREIGN KEY (`mark_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`order_address_id`) REFERENCES `addresses` (`address_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`order_status_id`) REFERENCES `statuses` (`status_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
