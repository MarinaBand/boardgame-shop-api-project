<?php
class Auth
{
    private $connection;
    public function __construct()
    {
        $this->connection = DB::getConnection();
    }

    public function checkUserByEmailAndPassword($email, $password)
    {
        $query = (new Select('users'))
            ->what(['user_id'])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'user_email', 'comparison' => '=', 'key2' => $email],
                ['clauseOrOperator' => 'AND', 'key1' => 'user_password', 'comparison' => '=', 'key2' => $password]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result)['user_id'] ?? 0;
    }

    public function authorize($userId)
    {
        $helper = new Helper();
        $token = $helper->generateToken();
        $tokenTime = time() + 3600; // в секундах (текущее время + 1 час)
        $query = "
            INSERT INTO `connects`
            SET `connect_user_id` = '$userId',
                `connect_token` = '$token',
                `connect_token_time` = FROM_UNIXTIME($tokenTime);
        ";
        mysqli_query($this->connection, $query);
        $query = (new Select('users'))
            ->what(['user_role_id', 'user_name'])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'user_id', 'comparison' => '=', 'key2' => $userId]
            ])
            ->build();
        $res = mysqli_query($this->connection, $query);
        $res = mysqli_fetch_assoc($res);
        $userRole = $res['user_role_id'];
        $userName = $res['user_name'];
        return ["uid" => $userId, "token" => $token, "tokenTime" => $tokenTime, "name" => $userName, "userRole" => $userRole];
    }
    public function logout($id)
    {
        $query = (new Delete('connects'))
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'connect_user_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        mysqli_query($this->connection, $query);
    }


    public function userIsAuthorized()
    {
        $isAuthorized = 0;
        if (isset($_COOKIE['uid']) && isset($_COOKIE['token']) && isset($_COOKIE['t_token'])) {
            $userId = $_COOKIE['uid'];
            $token = $_COOKIE['token'];
            $tokenTime = $_COOKIE['t_token'];
            $query = (new Select('connects'))
                ->what(['connect_id'])
                ->where([
                    ['clauseOrOperator' => 'WHERE', 'key1' => 'connect_user_id', 'comparison' => '=', 'key2' => $userId],
                    ['clauseOrOperator' => 'AND', 'key1' => 'connect_token', 'comparison' => '=', 'key2' => $token]
                ])
                ->build();

            $result = mysqli_query($this->connection, $query);
            if (mysqli_num_rows($result) > 0) {
                $query = (new Select('users'))
                    ->what(['user_role_id'])
                    ->where([
                        ['clauseOrOperator' => 'WHERE', 'key1' => 'user_id', 'comparison' => '=', 'key2' => $userId]
                    ])
                    ->build();
                $res = mysqli_query($this->connection, $query);
                $isAuthorized = mysqli_fetch_assoc($res)['user_role_id'];
                if ($tokenTime < time()) {
                    $helper = new Helper();
                    $newToken = $helper->generateToken();
                    $newTokenTime = time() + 1800;
                    setcookie('uid', $userId, time() + 2 * 24 * 60 * 60, '/');
                    setcookie('token', $newToken, time() + 2 * 24 * 60 * 60, '/');
                    setcookie('t_token', $newTokenTime, time() + 2 * 24 * 60 * 60, '/');
                    $connectId = mysqli_fetch_assoc($result)['connect_id'];
                    $query = "
                    UPDATE `connects`
                    SET `connect_token` = '$newToken',
                        `connect_token_time` = FROM_UNIXTIME($newTokenTime)
                    WHERE `connect_id` = $connectId;
                    ";
                    mysqli_query($this->connection, $query);
                }
            }
        }
        return $isAuthorized;
    }
    public function userAlreadyVoted($gameId)
    {
        if ($this->userIsAuthorized()) {
            $userId = $_COOKIE['uid'];
            $query = (new Select('marks'))
                ->where([
                    ['clauseOrOperator' => 'WHERE', 'key1' => 'mark_game_id', 'comparison' => '=', 'key2' => $gameId],
                    ['clauseOrOperator' => 'AND', 'key1' => 'mark_user_id', 'comparison' => '=', 'key2' => $userId]
                ])
                ->build();
            $result = mysqli_query($this->connection, $query);
            return mysqli_num_rows($result) !== 0;
        }
        return true;
    }
    public function getAllRoles()
    {
        $query = (new Select('roles'))
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}