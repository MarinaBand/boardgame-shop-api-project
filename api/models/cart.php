<?php

class Cart {
    private $connection;
    public function __construct(){
        $this->connection = DB::getConnection();
    }

    public function getList()
    {
        $query = (new Select('categories'))
            ->what(['SQL_CALC_FOUND_ROWS *'])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'category_is_deleted', 'comparison' => '=', 'key2' => 0]
            ])
            ->limit(['offset' => $offset, 'count' => $limit])
            ->build();
        $result = mysqli_query($this->connection, $query);
        $categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $result = mysqli_query($this->connection, "SELECT FOUND_ROWS()");
        $count = mysqli_fetch_row($result)[0];
        return array(
            'categories' => $categories,
            'count' => $count
        );
    }

    public function getAll()
    {
        $query = (new Select('categories'))
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'category_is_deleted', 'comparison' => '=', 'key2' => 0]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
    public function getGamesInCategory($id)
    {
        $query = (new Select('games'))
            ->what(['game_id', 'game_name', 'game_price', 'game_players_count_min', 'game_players_count_max', 'game_age_restrictions', 'game_description', 'category_name'])
            ->join([
                ['type' => 'LEFT', 'table' => 'games_categories', 'key1' => 'game_id', 'key2' => 'game_category_game_id'],
                ['type' => 'LEFT', 'table' => 'categories', 'key1' => 'game_category_category_id', 'key2' => 'category_id']
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'game_is_deleted', 'comparison' => '=', 'key2' => 0],
                ['clauseOrOperator' => 'AND', 'key1' => 'category_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->group(['game_id'])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
    public function addCategory($name)
    {
        $name = mysqli_real_escape_string($this->connection, $name);
        $query = (new Insert('categories'))
            ->what(['category_name'])
            ->values([$name])
            ->build();
        mysqli_query($this->connection, $query);
    }
    public function getById($id)
    {
        $query = (new Select('categories'))
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'category_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result);
    }
    public function editCategory($name, $id)
    {
        $name = mysqli_real_escape_string($this->connection, $name);
        $query = (new Update('categories'))
            ->values([
                ['column' => 'category_name', 'expression' => $name]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'category_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        return mysqli_query($this->connection, $query);
    }
    public function deleteCategory($id)
    {
        $query = (new Update('categories'))
            ->values([
                ['column' => 'category_is_deleted', 'expression' => 1]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'category_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        return mysqli_query($this->connection, $query);
    }
}