<?php
class Mark {
    private $connection;
    public function __construct(){
        $this->connection = DB::getConnection();
    }
    public function getAll()
    {
        $query = (new Select('marks'))
            ->what(['id' => 'mark_id', 'value' => 'mark_value', 'gameId' => 'game_id', 'gameName' => 'game_name', 'userId' => 'mark_user_id', 'userName' => 'user_name', 'comment' => 'mark_comment', 'moderated' => 'mark_is_moderated'])
            ->join([
                ['type' => 'LEFT', 'table' => 'games', 'key1' => 'mark_game_id', 'key2' => 'game_id'],
                ['type' => 'LEFT', 'table' => 'users', 'key1' => 'mark_user_id', 'key2' => 'user_id']
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getById($id)
    {
        $query = (new Select('marks'))
            ->what(['id' => 'mark_id', 'value' => 'mark_value', 'gameId' => 'game_id', 'gameName' => 'game_name', 'userId' => 'mark_user_id', 'userName' => 'user_name', 'comment' => 'mark_comment', 'moderated' => 'mark_is_moderated'])
            ->join([
                ['type' => 'LEFT', 'table' => 'games', 'key1' => 'mark_game_id', 'key2' => 'game_id'],
                ['type' => 'LEFT', 'table' => 'users', 'key1' => 'mark_user_id', 'key2' => 'user_id']
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'mark_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result);
    }

    public function addMark($userId, $gameId, $value, $comment)
    {
        $query = (new Insert('marks'))
            ->what(['mark_game_id', 'mark_user_id', 'mark_value', 'mark_comment', 'mark_date', 'mark_is_moderated'])
            ->values([$gameId, $userId, $value, $comment, date("Y-m-d"), 0])
            ->build();
        return mysqli_query($this->connection, $query);
    }

    public function editMark($data, $id)
    {
        $query = (new Update('marks'))
            ->values([
                ['column' => 'mark_game_id', 'expression' => $data['game_id']],
                ['column' => 'mark_value', 'expression' => $data['mark_value']],
                ['column' => 'mark_comment', 'expression' => $data['mark_comment']],
                ['column' => 'mark_is_moderated', 'expression' => 1]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'mark_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        mysqli_query($this->connection, $query);
        return;
    }
    public function deleteMark($id)
    {
        $query = (new Update('games'))
            ->values([
                ['column' => 'mark_is_deleted', 'expression' => 1]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'game_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        return mysqli_query($this->connection, $query);
    }
}