<?php
class Game {
    private $connection;
    public function __construct(){
        $this->connection = DB::getConnection();
    }
    public function getAll()
    {
    $query = (new Select('games'))
        ->what(['id' => 'game_id', 'name' => 'game_name', 'price' => 'game_price', 'playersMin' => 'game_players_count_min', 'playersMax' => 'game_players_count_max', 'ageRestrictions' =>'game_age_restrictions', 'description' => 'game_description', 'imageName' => 'game_img_name', 'inStock' => 'game_count', 'averageMark' => 'game_average_mark', 'categories' => 'GROUP_CONCAT(`category_name` SEPARATOR ", ")', 'categoriesIds' => 'GROUP_CONCAT(`category_id` SEPARATOR ", ")'])
        ->join([
            ['type' => 'LEFT', 'table' => 'games_categories', 'key1' => 'game_id', 'key2' => 'game_category_game_id'],
            ['type' => 'LEFT', 'table' => 'categories', 'key1' => 'game_category_category_id', 'key2' => 'category_id']
        ])
        ->where([
            ['clauseOrOperator' => 'WHERE', 'key1' => 'game_is_deleted', 'comparison' => '=', 'key2' => 0]
        ])
        ->group(['game_id'])
        ->build();
    $result = mysqli_query($this->connection, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
    public function addGame($data)
    {
        $query = (new Insert('games'))
            ->what(['game_name', 'game_price', 'game_players_count_min', 'game_players_count_max', 'game_age_restrictions', 'game_count', 'game_description', 'game_img_name'])
            ->values([$data['name'], $data['price'], $data['playersCountMin'], $data['playersCountMax'], $data['ageRestrictions'], $data['count'], $data['description'], $data['imgName']])
            ->build();
        mysqli_query($this->connection, $query);
        $game_id = mysqli_insert_id($this->connection);
        $query = "
            INSERT INTO `games_categories` (`game_category_game_id`, `game_category_category_id`) VALUES 
           ";
        foreach ($data['selectedCategories'] as $category) {
            $query .= "($game_id, $category), ";
        }
        $query = rtrim($query, ", ");
        $query .= ";";
        return mysqli_query($this->connection, $query);
    }
    public function getById($id)
    {
        $query = (new Select('games'))
            ->what(['id' => 'game_id', 'name' => 'game_name', 'price' => 'game_price', 'playersMin' => 'game_players_count_min', 'playersMax' => 'game_players_count_max', 'ageRestrictions' =>'game_age_restrictions', 'description' => 'game_description', 'imageName' => 'game_img_name', 'inStock' => 'game_count', 'averageMark' => 'game_average_mark', 'categories' => 'GROUP_CONCAT(`category_name` SEPARATOR ", ")', 'categoriesIds' => 'GROUP_CONCAT(`category_id` SEPARATOR ", ")'])
            ->join([
                ['type' => 'LEFT', 'table' => 'games_categories', 'key1' => 'game_id', 'key2' => 'game_category_game_id'],
                ['type' => 'LEFT', 'table' => 'categories', 'key1' => 'game_category_category_id', 'key2' => 'category_id']
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'game_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result);
    }
    public function editGame($data, $oldData, $id)
    {
        print_r($data);
        print_r($oldData);
        print_r($id);
        $query = (new Update('games'))
            ->values([
                ['column' => 'game_name', 'expression' => $data['name']],
                ['column' => 'game_price', 'expression' => $data['price']],
                ['column' => 'game_players_count_min', 'expression' => $data['playersMin']],
                ['column' => 'game_players_count_max', 'expression' => $data['playersMax']],
                ['column' => 'game_age_restrictions', 'expression' => $data['ageRestrictions']],
                ['column' => 'game_count', 'expression' => $data['inStock']],
                ['column' => 'game_description', 'expression' => $data['description']],
                ['column' => 'game_img_name', 'expression' => $data['imgName']]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'game_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        mysqli_query($this->connection, $query);

        if ($oldData['categories'] !== $data['selectedCategories']) {
            $deleteCategories = array_diff($oldData['categories'], $data['selectedCategories']);
            $newCategories = array_diff($data['selectedCategories'], $oldData['categories']);
            if ($deleteCategories) {
                $query = "
                        DELETE FROM `games_categories`
                        WHERE `game_category_game_id` = $id
                        AND `game_category_category_id` IN (
                    ";
                foreach ($deleteCategories as $deleteCategory) {
                    $query .= "$deleteCategory, ";
                }
                $query = rtrim($query, ", ");
                $query .= ");";
                mysqli_query($this->connection, $query);
            }
            if ($newCategories) {
                $query = "
                    INSERT INTO `games_categories` (`game_category_game_id`, `game_category_category_id`) VALUES
                ";
                foreach ($newCategories as $newCategory) {
                    $query .= "($id, $newCategory), ";
                }
                $query = rtrim($query, ", ");
                mysqli_query($this->connection, $query);
            }
        }
        return;
    }
    public function deleteGame($id)
    {
        $query = (new Update('games'))
            ->values([
                ['column' => 'game_is_deleted', 'expression' => 1]
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'game_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->build();
        return mysqli_query($this->connection, $query);
    }
    public function mark($userId, $gameId, $value, $data)
    {
        $query = (new Insert('marks'))
            ->what(['mark_game_id', 'mark_user_id', 'mark_value', 'mark_comment', 'mark_date', 'mark_is_moderated'])
            ->values([$gameId, $userId, $value, $data, date("Y-m-d"), 0])
            ->build();
        echo $query;
        return mysqli_query($this->connection, $query);
    }

    public function getGameModeratedMarks($id)
    {
        $query = (new Select('marks'))
            ->what(['mark_value', 'user_name', 'mark_comment', 'mark_date'])
            ->join([
                ['type' => 'LEFT', 'table' => 'games', 'key1' => 'mark_game_id', 'key2' => 'game_id'],
                ['type' => 'LEFT', 'table' => 'users', 'key1' => 'mark_user_id', 'key2' => 'user_id']
            ])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'mark_is_moderated', 'comparison' => '=', 'key2' => 1],
                ['clauseOrOperator' => 'AND', 'key1' => 'mark_game_id', 'comparison' => '=', 'key2' => $id]
            ])
            ->order([
                ['column' => 'mark_date', 'direction' => 'DESC']
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}