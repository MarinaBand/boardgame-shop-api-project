<?php
class Register
{
    private $connection;
    public function __construct()
    {
        $this->connection = DB::getConnection();
    }

    public function checkIfEmailExists($email)
    {
        $query = (new Select('users'))
            ->what(['COUNT' => 'COUNT(*)'])
            ->where([
                ['clauseOrOperator' => 'WHERE', 'key1' => 'user_email', 'comparison' => '=', 'key2' => $email]
            ])
            ->build();
        $result = mysqli_query($this->connection, $query);
        return mysqli_fetch_assoc($result)['COUNT'];
    }
    public function register($data)
    {
        if ($data['phone'] != '') {
            $query = (new Insert('users'))
                ->what(['user_login', 'user_email', 'user_password', 'user_name', 'user_phone', 'user_role_id'])
                ->values([$data['email'], $data['email'], $data['password'], $data['name'], $data['phone'], $data['role'],])
                ->build();
        } else {
            $query = (new Insert('users'))
                ->what(['user_login', 'user_email', 'user_password', 'user_name', 'user_role_id'])
                ->values([$data['email'], $data['email'], $data['password'], $data['name'], $data['role'],])
                ->build();
        }
        mysqli_query($this->connection, $query);
        $id = mysqli_insert_id($this->connection);
        return $id;
    }
}