<?php
$routes = array(
    'GameController' => array(
        'games/([0-9]*)' => 'main/$1'
    ),
    'AuthController' => array(
        'auth/([0-9]*)' => 'main/$1'
    ),
    'CategoryController' => array(
        'categories/([0-9]*)' => 'main/$1'
    ),
    'RegisterController' => array(
        'reg/([0-9]*)' => 'main/$1'
    ),
    'MarkController' => array(
        'marks/([0-9]*)' => 'main/$1'
    ),
    'CartController' => array(
        'cart' => 'main'
    ),
);