<?php
abstract class BaseController
{
    protected $answer;

    abstract function main($params);

    protected function showAnswer()
    {
        $this->addCorsHeaders();
        header("HTTP/1.1 200 OK");
        echo json_encode($this->answer);
    }

    protected function showNotFound()
    {
        $this->addCorsHeaders();
        header("HTTP/1.1 404 Not found");
    }

    protected function showUnauthorized()
    {
        $this->addCorsHeaders();
        header("HTTP/1.1 403 Unauthorized");
    }

    protected function showBadRequest()
    {
        $this->addCorsHeaders();
        header("HTTP/1.1 401 Bad request");
    }

    private function addCorsHeaders() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header("Access-Control-Allow-Headers: X-Requested-With");
    }

    protected function options() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        header('Access-Control-Max-Age: 1728000');
        header('Content-Length: 0');
        header('Content-Type: text/plain');
    }
}
