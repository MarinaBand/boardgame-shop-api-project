<?php

class CartController
{
//    private $categoryModel;
    private $cartModel;
    private $connection;
    public $isAuthorized;
    public function __construct()
    {
//        $this->categoryModel = new Category();
        $this->cartModel = new Cart();
        $this->connection = DB::getConnection();
        $this->isAuthorized = (new User())->userIsAuthorized();
    }
    public function actionIndex()
    {
        $title = 'Корзина';
        if (isset($_POST['item'])) {
            $item = htmlentities($_POST['item']);
            $item = mysqli_real_escape_string($this->connection, $item);
            echo $item;
            $itemArray = array(
                'item_id' => $item,
                'quantity' => 1
            );
            $itemData[] = $itemArray;
            $itemData = json_encode($itemData);
            setcookie('cart', $itemData, time() + (86400 * 30));
//            header();
            if (isset($_COOKIE['cart'])) {
                $total = 0;
                $cookieData = stripcslashes($_COOKIE['cart']);
                $cartData = json_decode($cookieData, true);
                foreach ($cartData as $item) {
                    print_r($item);
                }
            }

            // let cartData = JSON.stringify(itemObject);
        }

        include_once ('./views/cart/index.php');
    }

    public function actionAdd()
    {
        $title = 'Добавить категорию';
        if(isset($_POST['category_name'])) {
            $name = htmlentities($_POST['category_name']);
            $this->categoryModel->addCategory($name);
            header('Location: ' . FULL_SITE_ROOT . 'categories');
        }
        include_once ('./views/category/form.php');
    }

    public function actionEdit($id)
    {
        $title = 'Изменить категорию';

        if (!isset($id)) {
            echo 'Страница не найдена';
            exit();
        }
        if (isset($_POST['category_name'])) {
            $name = htmlentities($_POST['category_name']);
            $this->categoryModel->editCategory($name, $id);
            header('Location: ' . FULL_SITE_ROOT . 'categories');
        }
        $category = $this->categoryModel->getById($id);
        include_once ('./views/category/form.php');
    }

    public function actionDelete($id)
    {
        if (!isset($id)) {
            echo 'Страница не найдена';
            exit();
        }
        $this->categoryModel->deleteCategory($id);
        header('Location: ' . FULL_SITE_ROOT . 'categories');

    }

    public function actionView($id)
    {

        if (!isset($id)) {
            echo 'Страница не найдена';
            exit();
        }
        $games = $this->categoryModel->getGamesInCategory($id);
        $category = $this->categoryModel->getById($id);
        $title = $category['category_name'];
        include_once ('./views/category/view.php');
    }
}