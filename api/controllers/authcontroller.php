<?php
class AuthController extends BaseController
{
    private $authModel;

    public function __construct()
    {
        $this->authModel = new Auth();
    }

    public function main($params)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case "POST":
                $this->post();
                break;
            case "DELETE":
                $this->delete($params);
                break;
            case "OPTIONS":
                $this->options();
                break;
            default:
                $this->showBadRequest();
        }
    }

    protected function post()
    {
        $param = file_get_contents("php://input");
        $data = json_decode($param, true);

        $email = htmlentities($data['email']);
        $password = htmlentities($data['password']);
        $userId = $this->authModel->checkUserByEmailAndPassword($email, $password);
        if ($userId > 0) {
            $result = $this->authModel->authorize($userId);
            $this->answer = $result;
        } else {
            $this->answer = ["error" => "Неверный адрес электронной почты или пароль"];
        }
        $this->showAnswer();
    }
    protected function delete($id)
    {
        $this->authModel->logout($id);
        $this->answer = 0;
        $this->showAnswer();
    }
}