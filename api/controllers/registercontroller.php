<?php

class RegisterController extends BaseController
{
    private $registerModel;
    private $connection;
    private $authModel;
//    public $isAuthorized;
    public function __construct()
    {
        $this->registerModel = new Register();
        $this->connection = DB::getConnection();
        $this->authModel = new Auth();
//        $this->isAuthorized = (new User())->userIsAuthorized();
    }

    public function main($params)
    {
        $method = $_SERVER['REQUEST_METHOD'];
//        if (!isset($_GET['token']) || $_GET['token'] == '') {
//            $this->showUnauthorized();
//            die();
//        }
//        if ($this->authModel->checkToken($_GET['token'])) {
        //echo 'lalala';
        switch ($method){
            case "GET":
                $this->get();
                break;
            case "POST":
                $this->post();
                break;
            case "OPTIONS":
                $this->options();
                break;
            default:
                $this->showBadRequest();
        }
//        } else {
//            $this->showUnauthorized();
//        }

    }

    protected function post()
    {
        $param = file_get_contents("php://input");
        $data = json_decode($param, true);
        //TODO: сделать проверки на регулярные выражения

        if ($this->registerModel->checkIfEmailExists($data['email']) > 0) {
            $this->answer = ["error" => "Пользователь с таким email уже существует"];
        } else {
            $id = $this->registerModel->register($data);

//            $this->answer = $id;
            $result = $this->authModel->authorize($id);
            $this->answer = $result;
        }
        $this->showAnswer();
    }
}