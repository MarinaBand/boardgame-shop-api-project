<?php

class CategoryController extends BaseController
{
    private $categoryModel;
    private $connection;
    public $isAuthorized;
    public function __construct()
    {
        $this->categoryModel = new Category();

    }

    public function main($param)
    {
        $method = $_SERVER['REQUEST_METHOD'];
//        if (!isset($_GET['token']) || $_GET['token'] == '') {
//            $this->showUnauthorized();
//            die();
//        }
//        if ($this->authModel->checkToken($_GET['token'])) {
        switch ($method){
            case "GET":
                $this->get($param);
                break;
            case "POST":
                $this->post();
                break;
            case "PUT":
                $this->put($param);
                break;
            case "DELETE":
                $this->delete($param);
                break;
            case "OPTIONS":
                $this->options();
                break;
            default:
                $this->showBadRequest();
        }
//        } else {
//            $this->showUnauthorized();
//        }

    }

    protected function get($id)
    {
        if($id>0) {
            $category = $this->categoryModel->getById($id);
            $this->answer = $category;
            $this->showAnswer();
        } else {
            $categories = $this->categoryModel->getAll();
            $this->answer = $categories;
            $this->showAnswer();

        }
    }

    protected function post()
    {
        $param = file_get_contents("php://input");
        $data = json_decode($param, true);
        $name = htmlentities($data['name']);
        if ($this->categoryModel->addCategory($name)) {
            $this->answer = 0;
        } else {
            $this->answer = ["error" => "Не удалось добавить категорию"];
        }
        $this->showAnswer();
    }

    protected function put($id)
    {
        $param = file_get_contents("php://input");
        $data = json_decode($param, true);
        $name = htmlentities($data['name']);
        if ($this->categoryModel->editCategory($name, $id)) {
            $this->answer = 0;
        } else {
            $this->answer = ["error" => "Не удалось добавить категорию"];
        }
        $this->showAnswer();
    }

    protected function delete($id)
    {
        if ($this->categoryModel->deleteCategory($id)) {
            $this->answer = 0;
        } else {
            $this->answer = ["error" => "Не удалось удалить категорию"];
        }
        $this->showAnswer();
    }

//    public function actionIndex($page = 1)
//    {
//        $title = 'Категории';
//        $limit = 3;
//        $offset = ($page - 1) * $limit;
//        $categoriesInfo = $this->categoryModel->getList($offset, $limit);
//        $categories = $categoriesInfo['categories'];
//        $count = $categoriesInfo['count'];
//
//        $pagination = new Pagination($count, $page, $limit, 'page=');
//        include_once ('./views/category/index.php');
//    }
//
//    public function actionAdd()
//    {
//        $title = 'Добавить категорию';
//        if(isset($_POST['category_name'])) {
//            $name = htmlentities($_POST['category_name']);
//            $name = mysqli_real_escape_string($this->connection, $name);
//            $this->categoryModel->addCategory($name);
//            header('Location: ' . FULL_SITE_ROOT . 'categories');
//        }
//        include_once ('./views/category/form.php');
//    }
//
//    public function actionEdit($id)
//    {
//        $title = 'Изменить категорию';
//
//        if (!isset($id)) {
//            echo 'Страница не найдена';
//            exit();
//        }
//        if (isset($_POST['category_name'])) {
//            $name = htmlentities($_POST['category_name']);
//            $name = mysqli_real_escape_string($this->connection, $name);
//            $this->categoryModel->editCategory($name, $id);
//            header('Location: ' . FULL_SITE_ROOT . 'categories');
//        }
//        $category = $this->categoryModel->getById($id);
//        include_once ('./views/category/form.php');
//    }
//
//    public function actionDelete($id)
//    {
//        if (!isset($id)) {
//            echo 'Страница не найдена';
//            exit();
//        }
//        $this->categoryModel->deleteCategory($id);
//        header('Location: ' . FULL_SITE_ROOT . 'categories');
//
//    }
//
//    public function actionView($id)
//    {
//
//        if (!isset($id)) {
//            echo 'Страница не найдена';
//            exit();
//        }
//        $games = $this->categoryModel->getGamesInCategory($id);
//        $category = $this->categoryModel->getById($id);
//        $title = $category['category_name'];
//        include_once ('./views/category/view.php');
//    }
}