<?php

class MarkController extends BaseController
{
    private $markModel;

    public function __construct()
    {
        $this->markModel = new Mark();
    }

    public function main($param)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case "GET":
                $this->get($param);
                break;
            case "POST":
                $this->post();
                break;
//            case "PUT":
//                $this->put($param);
//                break;
//            case "DELETE":
//                $this->delete($param);
//                break;
            case "OPTIONS":
                $this->options();
                break;
            default:
                $this->showBadRequest();
        }

    }

    protected function get($id)
    {
        if($id>0) {
            $mark = $this->markModel->getById($id);
            $this->answer = $mark;
            $this->showAnswer();
        } else {
            $marks = $this->markModel->getAll();
            $this->answer = $marks;
            $this->showAnswer();
        }
    }
    protected function post()
    {
        $param = file_get_contents("php://input");
        $data = json_decode($param, true);
        $comment = htmlentities($data['comment']);
        $value = htmlentities($data['value']);
        $gameId = htmlentities($data['gameId']);
        $userId = htmlentities($data['userId']);

        if ($this->markModel->addMark($userId, $gameId, $value, $comment)) {
            $this->answer = 0;
        } else {
            $this->answer = ["error" => "Не удалось добавить категорию"];
        }
        $this->showAnswer();
    }

}