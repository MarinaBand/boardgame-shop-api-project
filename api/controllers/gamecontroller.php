<?php

class GameController extends BaseController
{
    private $gameModel;
    private $categoryModel;


    public function __construct()
    {
        $this->gameModel = new Game();
        $this->categoryModel = new Category();
    }

    public function main($param)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method){
            case "GET":
                $this->get($param);
                break;
            case "POST":
                $this->post();
                break;
            case "PUT":
                $this->put($param);
                break;
            case "DELETE":
                $this->delete($param);
                break;
            case "OPTIONS":
                $this->options();
                break;
            default:
                $this->showBadRequest();
        }

    }

    protected function get($id)
    {
        if($id>0) {
            $game = $this->gameModel->getById($id);
            $this->answer = $game;
            $this->showAnswer();
        } else {
            $games = $this->gameModel->getAll();
            $this->answer = $games;
            $this->showAnswer();
        }
    }

    protected function post()
    {
        $param = file_get_contents("php://input");
        json_decode($param, true);

        $name = htmlentities($_POST['name']);
        $price = htmlentities($_POST['price']);
        $playersCountMin = htmlentities($_POST['playersMin']);
        $playersCountMax = htmlentities($_POST['playersMax']);
        $ageRestrictions = htmlentities($_POST['ageRestrictions']);
        $count = htmlentities($_POST['inStock']);
        $description = htmlentities($_POST['description']);
        $selectedCategories = explode(',', $_POST['categoriesIds']);
        $gameImg = $_FILES["imgFile"];
        $gameImgName = $gameImg["name"];
        if (!preg_match('/^[0-9a-zа-я\s\:\-]+$/iu', $name)) {
            $errors[] = ["error" => "Название введено некорректно"];
        }
        if (!preg_match('/^[0-9]{0,5}$/', $price)) {
            $errors[] = ["error" => "Цена введена некорректно"];
        }
        if (!preg_match('/^[0-9]{0,2}$/', $playersCountMin)) {
            $errors[] = ["error" => "Минимальное число игровоков введено некорректно"];
        }
        if (!preg_match('/^[0-9]{0,2}$/', $playersCountMax)) {
            $errors[] = ["error" => "Максимальное число игровоков введено некорректно"];
        }
        if (!preg_match('/^[0-9]{0,2}$/', $ageRestrictions)) {
            $errors[] = ["error" => "Возрастные ограничения введены некорректно"];
        }
        if (!preg_match('/^[0-9]{0,5}$/', $count)) {
            $errors[] = ["error" => "Количество введено некорректно"];
        }
        if (!preg_match('/(.){0,255}/iu', $description)) {
            $errors[] = ["error" => "Слишком длинное описание"];
        }
        if ($gameImg['size'] >= 5000000) {
            $errors[] = ["error" => "Превышен максимальный размер файла изображения"];
        }
        if ($gameImg['type'] !== 'image/pjpeg' && $gameImg['type'] !== 'image/jpeg' && $gameImg['type'] !== 'image/png') {
            $errors[] = ["error" => "Некорректный формат файла изображения"];
        }
        if (empty($errors)) {
            move_uploaded_file($gameImg["tmp_name"], "./assets/img/$gameImgName");
            $game = array(
                'name' => $name,
                'price' => $price,
                'playersCountMin' => $playersCountMin,
                'playersCountMax' => $playersCountMax,
                'ageRestrictions' => $ageRestrictions,
                'count' => $count,
                'description' => $description,
                'selectedCategories' => $selectedCategories,
                'imgName' => $gameImgName
            );
            if ($this->gameModel->addGame($game)) {
                $this->answer = 0;
            } else {
                $this->answer = ["error" => "Не удалось добавить категорию"];
            }
        } else {
            $this->answer = $errors;
        }
        $this->showAnswer();
    }

//    public function actionIndex()
//    {
//        $title = 'Игры';
//        $games = $this->gameModel->getAll();
//        include_once ('./views/game/index.php');
//    }
//
//    public function actionAdd()
//    {
//        $title = 'Добавить игру';
//        $errors = [];
//        if(isset($_POST['game_name'])) {
//            $name = htmlentities($_POST['game_name']);
//            $name = mysqli_real_escape_string($this->connection, $name);
//            $price = htmlentities($_POST['game_price']);
//            $price = mysqli_real_escape_string($this->connection, $price);
//            $playersCountMin = htmlentities($_POST['game_players_count_min']);
//            $playersCountMin = mysqli_real_escape_string($this->connection, $playersCountMin);
//            $playersCountMax = htmlentities($_POST['game_players_count_max']);
//            $playersCountMax = mysqli_real_escape_string($this->connection, $playersCountMax);
//            $ageRestrictions = htmlentities($_POST['game_age_restrictions']);
//            $ageRestrictions = mysqli_real_escape_string($this->connection, $ageRestrictions);
//            $weight = htmlentities($_POST['game_weight']);
//            $weight = mysqli_real_escape_string($this->connection, $weight);
//            $count = htmlentities($_POST['game_count']);
//            $count = mysqli_real_escape_string($this->connection, $count);
//            $description = htmlentities($_POST['game_description']);
//            $description = mysqli_real_escape_string($this->connection, $description);
//            $selectedCategories = $_POST['game_category'];
//            $gameImg = $_FILES["game_img"];
//            $gameImgName = $gameImg["name"];
//
//            if (!preg_match('/^[0-9a-zа-я\s\:\-]+$/iu', $name)) {
//                $errors[] = 'Название введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $price)) {
//                $errors[] = 'Цена введена некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $playersCountMin)) {
//                $errors[] = 'Минимальное число игровоков введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $playersCountMax)) {
//                $errors[] = 'Максимальное число игровоков введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $ageRestrictions)) {
//                $errors[] = 'Возрастные ограничения введены некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $weight)) {
//                $errors[] = 'Вес введен некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $count)) {
//                $errors[] = ' Количество введено некорректно<br>';
//            }
//            if (!preg_match('/(.){0,255}/iu', $description)) {
//                $errors[] = 'Слишком длинное описание<br>';
//            }
//            if ($gameImg['size'] >= 5000000) {
//                $errors[] = 'Превышен максимальный размер файла изображения<br>';
//            }
//            if ($gameImg['type'] !== 'image/pjpeg' && $gameImg['type'] !== 'image/jpeg' && $gameImg['type'] !== 'image/png') {
//                $errors[] = 'Некорректный формат файла изображения<br>';
//            }
//
//            if (empty($errors)) {
//                move_uploaded_file($gameImg["tmp_name"], "./assets/img/$gameImgName");
//                $this->gameModel->addGame(array(
//                    'name' => $name,
//                    'price' => $price,
//                    'playersCountMin' => $playersCountMin,
//                    'playersCountMax' => $playersCountMax,
//                    'ageRestrictions' => $ageRestrictions,
//                    'weight' => $weight,
//                    'count' => $count,
//                    'description' => $description,
//                    'selectedCategories' => $selectedCategories,
//                    'imgName' => $gameImgName
//                ));
//                header('Location: ' . FULL_SITE_ROOT . 'games');
//            }
//
//        }
//        $categories = $this->categoryModel->getAll();
//        include_once ('./views/game/form.php');
//    }
//
//    public function actionEdit($id)
//    {
//        if (!isset($id)) {
//            echo 'Страница не найдена';
//            exit();
//        }
//        $title = 'Редактировать игру';
//        $errors = [];
//        $game = $this->gameModel->getById($id);
//        $game['categories'] = explode(',', $game['categories']);
//
//        if (isset($_POST['game_name'])) {
//            $name = htmlentities($_POST['game_name']);
//            $name = mysqli_real_escape_string($this->connection, $name);
//            $price = htmlentities($_POST['game_price']);
//            $price = mysqli_real_escape_string($this->connection, $price);
//            $playersCountMin = htmlentities($_POST['game_players_count_min']);
//            $playersCountMin = mysqli_real_escape_string($this->connection, $playersCountMin);
//            $playersCountMax = htmlentities($_POST['game_players_count_max']);
//            $playersCountMax = mysqli_real_escape_string($this->connection, $playersCountMax);
//            $ageRestrictions = htmlentities($_POST['game_age_restrictions']);
//            $ageRestrictions = mysqli_real_escape_string($this->connection, $ageRestrictions);
//            $weight = htmlentities($_POST['game_weight']);
//            $weight = mysqli_real_escape_string($this->connection, $weight);
//            $count = htmlentities($_POST['game_count']);
//            $count = mysqli_real_escape_string($this->connection, $count);
//            $description = htmlentities($_POST['game_description']);
//            $description = mysqli_real_escape_string($this->connection, $description);
//            $selectedCategories = $_POST['game_category'];
//            if (isset($_FILES["game_img"])) {
//                $oldImgName = $game['game_img_name'];
//                $gameImg = $_FILES["game_img"];
//                $gameImgName = $gameImg["name"];
//                if ($gameImg['size'] >= 5000000) {
//                    $errors[] = 'Превышен максимальный размер файла изображения<br>';
//                }
//                if ($gameImg['type'] !== 'image/pjpeg' && $gameImg['type'] !== 'image/jpeg' && $gameImg['type'] !== 'image/png') {
//                    $errors[] = 'Некорректный формат файла изображения<br>';
//                }
//                unlink("./assets/img/$oldImgName");
//                move_uploaded_file($gameImg["tmp_name"], "./assets/img/$gameImgName");
//            } else {
//                $gameImgName = $game['game_img_name'];
//            }
//
//            if (!preg_match('/^[0-9a-zа-я\s\:\-]+$/iu', $name)) {
//                $errors[] = 'Название введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $price)) {
//                $errors[] = 'Цена введена некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $playersCountMin)) {
//                $errors[] = 'Минимальное число игровоков введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $playersCountMax)) {
//                $errors[] = 'Максимальное число игровоков введено некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,2}$/', $ageRestrictions)) {
//                $errors[] = 'Возрастные ограничения введены некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $weight)) {
//                $errors[] = 'Вес введен некорректно<br>';
//            }
//            if (!preg_match('/^[0-9]{0,5}$/', $count)) {
//                $errors[] = ' Количество введено некорректно<br>';
//            }
//            if (!preg_match('/(.){0,255}/iu', $description)) {
//                $errors[] = 'Слишком длинное описание<br>';
//            }
//
//            if (empty($errors)) {
//                $this->gameModel->editGame(array(
//                    'name' => $name,
//                    'price' => $price,
//                    'playersCountMin' => $playersCountMin,
//                    'playersCountMax' => $playersCountMax,
//                    'ageRestrictions' => $ageRestrictions,
//                    'weight' => $weight,
//                    'count' => $count,
//                    'description' => $description,
//                    'selectedCategories' => $selectedCategories,
//                    'imgName' => $gameImgName
//                ), $game, $id);
////                header('Location: ' . FULL_SITE_ROOT . 'games');
//            }
//        }
//        $categories = $this->categoryModel->getAll();
//        include_once ('./views/game/form.php');
//    }
//
//    public function actionDelete($id)
//    {
//        if (!isset($id)) {
//            echo 'Страница не найдена';
//            exit();
//        }
//        $this->gameModel->deleteGame($id);
//        header('Location: ' . FULL_SITE_ROOT . 'games');
//
//    }
//
//    public function actionView($id)
//    {
//        $title = '';
//        $game = $this->gameModel->getById($id);
//        $userIsAlreadyVoted = $this->userModel->userAlreadyVoted($id);
//        $marks = $this->gameModel->getGameModeratedMarks($id);
//        include_once ('./views/game/view.php');
//    }
//
//    public function actionMark($gameId, $value, $data)
//    {
//        if (!$this->isAuthorized) {
//            echo 'Недостаточно прав для данной операции';
//        } else {
//            $userId = $_COOKIE['uid'];
//
//            if ($this->gameModel->mark($userId, $gameId, $value, $data)) {
//                echo "Спасибо за вашу оценку";
//            } else {
//                echo "Не удалось добавить оценку";
//            };
//        }
//    }
}