<?php
class Delete
{
    private $table;
    private $where;
    public function __construct($table)
    {
        $this->table = $table;
    }

    public function where($wheres)
    {
        $str = "";
        foreach ($wheres as $where) {
            $str .= "$where[clauseOrOperator] `$where[key1]` $where[comparison] '$where[key2]' ";
        }
        $this->where = $str;
        return $this;
    }
    public function build()
    {
        $str ="
            DELETE FROM `$this->table`
            $this->where;
        ";
        return $str;
    }
}