<?php
class Update
{
    private $table;
    private $values;
    private $where;
    public function __construct($table)
    {
        $this->table = $table;
    }

    public function values($values)
    {
        $str = "SET ";
        foreach ($values as $value) {
            $str .= "`$value[column]` = '$value[expression]', ";
        }
        $str = rtrim($str, ", ");
        $this->values = $str;
        return $this;
    }
    public function where($wheres)
    {
        $str = "";
        foreach ($wheres as $where) {
            $str .= "$where[clauseOrOperator] `$where[key1]` $where[comparison] '$where[key2]' ";
        }
        $this->where = $str;
        return $this;
    }
    public function build()
    {
        $str ="
            UPDATE `$this->table`
            $this->values
            $this->where;
        ";
        return $str;
    }
}