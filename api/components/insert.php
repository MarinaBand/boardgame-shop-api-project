<?php
class Insert
{
    private $into;
    private $what;
    private $values;

    public function __construct($into)
    {
        $this->into = $into;
    }
    public function what($what)
    {
        $str = "";
        foreach ($what as $value){
            $str .= "`$value`, ";
        }
        $str = rtrim($str, ", ");
        $this->what = $str;
        return $this;
    }
    public function values($values)
    {
        $str = "";
        foreach ($values as $value) {
            $str .= "'$value', ";
        }
        $str = rtrim($str, ", ");
        $this->values = $str;
        return $this;
    }

    public function build()
    {
        $str ="
            INSERT INTO `$this->into` ($this->what)
            VALUES ($this->values);
        ";
        return $str;
    }
}