import { Component, OnInit } from '@angular/core';
import {MessengerService} from "../../../services/messenger.service";
import { CartItem } from "../../../models/cart-item";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  cartTotal = 0;
  isAuthorized = 0;
  userName = '';

  constructor(
    private message: MessengerService,
    private userService: UserService
  ) {
    if (localStorage.getItem('CART') !== null) {
      JSON.parse(<string>localStorage.getItem('CART')).forEach((item: CartItem) => {
        this.cartTotal += item.quantity
      })
    }
    this.isAuthorized = this.userService.isAuthorized();
    this.userName = <string>localStorage.getItem('USER_NAME');
  }

  ngOnInit(): void {
    this.message.getQtyToHeader().subscribe((i: number) => {
      this.cartTotal = i;
    })
    this.message.getIsAuthorized().subscribe((res) => {
      this.isAuthorized = res;
      this.userName = <string>localStorage.getItem('USER_NAME');
    });
    this.checkCookiesNotification();
  }

  logOut() {
    this.userService.logOut(Number(localStorage.getItem('UID')))
      .subscribe((res) => {
        if (res === 0) {
          localStorage.removeItem('DATE_EXP');
          localStorage.removeItem('ACCESS_TOKEN');
          localStorage.removeItem('USER_NAME');
          localStorage.removeItem('USER_ROLE');
          localStorage.removeItem('UID');
          location.reload();
        }

      })
  }

  checkCookiesNotification(){
    let cookieDate = localStorage.getItem('cookieDate');
    let cookieNotificationElement = document.getElementById('cookie_notification');
    let cookieBtn = document.querySelector('.cookie_accept');
    //если cookieDate старше недели ли не существует, показываем плашку
    if( !cookieDate || (Number(cookieDate) + (86400 * 7 * 1000)) < Date.now() ){
      // @ts-ignore
      cookieNotificationElement.classList.add('show');
    }
    // @ts-ignore
    cookieBtn.addEventListener('click', function(){
      localStorage.setItem( 'cookieDate', String(Date.now()) );
      // @ts-ignore
      cookieNotificationElement.classList.remove('show');
    })
  }
}
