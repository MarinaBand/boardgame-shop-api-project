import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GamesComponent} from "./games/games.component";
import {CategoryComponent} from "./categories/category/category.component";
import {CartComponent} from "./cart/cart.component";
import {OrderComponent} from "./order/order.component";
import {NotFoundComponent} from "../../components/common/not-found/not-found.component";
import {GameViewComponent} from "./games/game-view/game-view.component";
import {AboutComponent} from "./about/about.component";

const routes: Routes = [
  { path: '', redirectTo: '/shop/games', pathMatch: 'full' },
  { path: 'games', component: GamesComponent },
  { path: 'games/:id', component: GameViewComponent },
  { path: 'categories/:id', component: CategoryComponent },
  { path: 'about', component: AboutComponent },
  { path: 'cart', component: CartComponent },
  { path: 'order', component: OrderComponent },
  { path: '**', component: NotFoundComponent },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
