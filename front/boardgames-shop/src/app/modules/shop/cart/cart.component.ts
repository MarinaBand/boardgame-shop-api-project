import { Component, OnInit } from '@angular/core';
import { MessengerService } from "../../../services/messenger.service";
import { Game } from "../../../models/game";
import { CartItem } from "../../../models/cart-item";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItems: CartItem[]
  displayedColumns: string[] = ['position', 'title', 'price', 'quantity', 'total', 'delete'];
  isAuthorized = 0;
  cartTotal = 0;
  cartQuantity = 0;

  constructor(
    private message: MessengerService,
    private userService: UserService
  ) {
    if (localStorage.getItem('CART') !== null) {
      this.cartItems = JSON.parse(<string>localStorage.getItem('CART'))
      this.cartItems.forEach(item => {
        this.cartTotal += (item.quantity * item.price)
      })
    } else {
      this.cartItems = []
    }
    this.isAuthorized = this.userService.isAuthorized();
  }

  ngOnInit(): void {
    this.message.getGameToCart().subscribe((game: Game) => {
      console.log(game);
      this.addToCart(game);
    })

  }

  addToCart(game: Game) {
    let gameInCart = false;
    for (let i in this.cartItems) {
      if (this.cartItems[i].gameId === game.id) {
        this.cartItems[i].quantity++;
        gameInCart = true;
        break;
      }
    }
    if(!gameInCart) {
      this.cartItems.push({
        gameId: <number>game.id,
        gameName: game.name,
        price: game.price,
        imageName: game.imageName,
        quantity: 1,
      })
    }
    this.cartTotal = 0;
    this.cartQuantity = 0;
    this.cartItems.forEach(item => {
      this.cartTotal += (item.quantity * item.price);
      this.cartQuantity += item.quantity
    })
    localStorage.setItem('CART', JSON.stringify(this.cartItems))
    this.message.sendQtyToHeader(this.cartQuantity);
  }

  remove(id: number): void {
    this.cartItems = this.cartItems.filter(
      (item: CartItem) => item.gameId !== id
    );
    this.cartTotal = 0;
    this.cartQuantity = 0;
    this.cartItems.forEach(item => {
      this.cartTotal += (item.quantity * item.price);
      this.cartQuantity += item.quantity
    })
    localStorage.setItem('CART', JSON.stringify(this.cartItems));
    this.message.sendQtyToHeader(this.cartQuantity);
  }
}
