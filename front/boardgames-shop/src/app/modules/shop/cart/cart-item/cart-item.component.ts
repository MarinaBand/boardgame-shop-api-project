import { Component, OnInit, Input } from '@angular/core';
import { CartItem } from "../../../../models/cart-item";

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {
  @Input() cartItem: CartItem



  constructor() {
    this.cartItem = {
      gameId: 0,
      gameName: '',
      price: 0,
      imageName: '',
      quantity: 0
    }
  }

  ngOnInit(): void {
  }

}
