import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {passwordValidator} from "../../user/register/validators/password-validator";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {CartItem} from "../../../models/cart-item";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  isAuthorized = 0;
  cartQuantity = 0;
  tomorrow = new Date()
  deliverySelect: number = 0;
  form: FormGroup = new FormGroup({
        address: new FormControl('', [Validators.required, Validators.pattern(/^[a-zа-я0-9,\s]+$/iu)])
  });

  constructor(
    private readonly router: Router,
    private userService: UserService
  ) {
    this.isAuthorized = this.userService.isAuthorized();
    if (localStorage.getItem('CART') !== null) {
      let cart: CartItem[] = JSON.parse(<string>localStorage.getItem('CART'))
      cart.forEach(item => {
        this.cartQuantity += item.quantity
      })
    } else {
      this.cartQuantity = 0
    }
  }

  ngOnInit(): void {
    const today = new Date()
    this.tomorrow = new Date(today)
    this.tomorrow.setDate(this.tomorrow.getDate() + 1)
  }

  showShopAddress() {
    this.deliverySelect = 1;
  }
  showAddressInput() {
    this.deliverySelect = 2;
  }

  submitDeliveryShop() {
    if (this.isAuthorized == 0) {
      this.router.navigate([`/user/auth`]);
    } else if (this.cartQuantity == 0) {
      this.router.navigate([`/shop/cart`]);
    } else {
      console.log('making order');
    }
  }

  submitDeliveryAddress() {
    if (this.isAuthorized == 0) {
      this.router.navigate([`/user/auth`]);
    } else if (this.cartQuantity == 0) {
      this.router.navigate([`/shop/cart`]);
    } else {
      console.log('making order');
    }
  }

}
