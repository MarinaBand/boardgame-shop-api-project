import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Game } from "../../../../models/game";
import { MessengerService } from "../../../../services/messenger.service";
import { UserService } from "../../../../services/user.service";

@Component({
  selector: 'app-game-list-item',
  templateUrl: './game-list-item.component.html',
  styleUrls: ['./game-list-item.component.css']
})
export class GameListItemComponent implements OnInit {

  @Input() gameItem: Game;

  imgUrl = '';

  constructor(
    private message: MessengerService,
    private cartService: UserService
  ) {
    this.imgUrl = environment.imgFolderUrl;
    this.gameItem = {
      id: 1,
      name: '',
      price: 0,
      playersMin: 0,
      playersMax: 0,
      ageRestrictions: 0,
      description: '',
      imageName: '',
      inStock: 0,
      categories: '',
      categoriesIds: ''
    }
  }

  ngOnInit(): void {
  }

  addToCart() {
    console.log(this.gameItem);
    this.message.sendGameToCart(this.gameItem);
  }

}
