import { Component, OnInit } from '@angular/core';
import {Game} from "../../../../models/game";
import {MessengerService} from "../../../../services/messenger.service";
import {environment} from "../../../../../environments/environment";
import {ActivatedRoute} from "@angular/router";
import {ShopService} from "../../../../services/shop.service";
import {Mark} from "../../../../models/mark";
import {UserService} from "../../../../services/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Error} from "../../../../models/error";

@Component({
  selector: 'app-game-view',
  templateUrl: './game-view.component.html',
  styleUrls: ['./game-view.component.css']
})
export class GameViewComponent implements OnInit {

  gameItem: Game;
  gameId: number;
  categories: string[];
  imgUrl = '';
  marksList: Mark[];
  mark: Mark;
  errors: Error[];
  isAuthorized = 0;
  form: FormGroup = new FormGroup({
    mark: new FormControl('',  Validators.required),
    comment: new FormControl('', [Validators.required, Validators.maxLength(250)])
  });

  constructor(
    private message: MessengerService,
    private shopService: ShopService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {
    this.imgUrl = environment.imgFolderUrl;
    this.gameId = 0;
    this.categories = [];
    this.marksList = [];
    this.mark = {
      value: 0,
      comment: '',
      gameId: 0,
      userId: 0,
      moderated: 0
    }
    this.errors = [];
    this.gameItem = {
      id: 1,
      name: '',
      price: 0,
      playersMin: 0,
      playersMax: 0,
      ageRestrictions: 0,
      description: '',
      imageName: '',
      inStock: 0,
      categories: '',
      categoriesIds: ''
    }

  }

  ngOnInit(): void {
    this.isAuthorized = this.userService.isAuthorized();
    this.route.params.subscribe(params => {
      this.gameId = params['id'];
      this.shopService.getGameById(params['id'])
        .subscribe((res) => {
          if ('error' in res) {
            console.log('ошибка');
          }
          if ('id' in res) {
            this.gameItem = res;
            this.categories = this.gameItem.categories.split(', ')
          }
        });
      this.shopService.getMarks()
        .subscribe((res) => {
          this.marksList = res.filter((mark) =>
            mark.moderated == 1 && mark.gameId == params['id']
          )
        })
    })

  }

  toCart(id: number) {
    this.message.sendGameToCart(this.gameItem)
}

  onMarkSave() {
    this.mark.value = this.form.value.mark;
    this.mark.comment = this.form.value.comment;
    this.mark.gameId = this.gameId;
    this.mark.userId = Number(localStorage.getItem('UID'));
    this.shopService.addMark(this.mark)
      .subscribe((res) => {
        if (res == 0) {
          // @ts-ignore
          document.querySelector('.new-comment-container').innerHTML = '<div class="alert alert-info">Спасибо за вашу оценку! Она появится здесь после проверки модератором</div>';
        }
        if (!Number(res)) {
          this.errors = [];
          this.errors.push(<Error><unknown>res)
        }
      })
  }
}
