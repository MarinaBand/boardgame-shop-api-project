import { Component, OnInit } from '@angular/core';
import { ShopService } from "../../../services/shop.service";
import { Game } from "../../../models/game";

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  gamesList: Game[] = []

  constructor(
    private shopService: ShopService
  ) { }

  ngOnInit(): void {
    this.shopService.getGames().subscribe((games) => {
      this.gamesList = games
    })
  }

}
