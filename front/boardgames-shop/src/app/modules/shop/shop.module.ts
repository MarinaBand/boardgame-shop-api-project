import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopRoutingModule } from './shop-routing.module';
import { GamesComponent } from './games/games.component';
import { CategoriesComponent } from './categories/categories.component';
import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { GameListItemComponent } from './games/game-list-item/game-list-item.component';
import { CategoryComponent } from './categories/category/category.component';
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import { OrderComponent } from './order/order.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import { GameViewComponent } from './games/game-view/game-view.component';
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import { AboutComponent } from './about/about.component';


@NgModule({
  declarations: [
    GamesComponent,
    CategoriesComponent,
    CartComponent,
    CartItemComponent,
    GameListItemComponent,
    CategoryComponent,
    OrderComponent,
    GameViewComponent,
    AboutComponent
  ],
    imports: [
        CommonModule,
        ShopRoutingModule,
        FontAwesomeModule,
        MatIconModule,
        MatTableModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        ReactiveFormsModule,
        MatButtonModule
    ]
})
export class ShopModule { }
