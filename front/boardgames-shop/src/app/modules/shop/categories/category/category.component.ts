import { Component, OnInit } from '@angular/core';
import { ShopService } from "../../../../services/shop.service";
import { Game } from "../../../../models/game";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  gamesList: Game[] = [];
  categoryId: number = 0

  constructor(
    private gameService: ShopService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.categoryId = params['id']
      this.gameService.getGames().subscribe((games) => {
        this.gamesList = games.filter((game) => {
          return game.categoriesIds.split(', ').includes(this.categoryId.toString())
        }

        )
      })
    });


  }

}
