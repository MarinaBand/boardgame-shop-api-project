import { Component, OnInit } from '@angular/core';
import { ShopService } from "../../../services/shop.service";
import { Category } from "../../../models/category";
import {Game} from "../../../models/game";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categoriesList: Category[] = []

  constructor(
    private shopService: ShopService
  ) { }

  ngOnInit(): void {
    this.shopService.getCategories().subscribe((categories) => {
      this.categoriesList = categories
    })
  }

}
