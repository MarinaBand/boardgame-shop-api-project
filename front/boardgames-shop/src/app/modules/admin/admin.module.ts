import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { GamesComponent } from './games/games.component';
import { GameUpdateComponent } from './game-update/game-update.component';
import { GameCreateComponent } from './game-create/game-create.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryCreateComponent } from './category-create/category-create.component';
import { CategoryUpdateComponent } from './category-update/category-update.component';
import { RegisterComponent } from './register/register.component';
import { MarksComponent } from './marks/marks.component';
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatSelectModule} from "@angular/material/select";


@NgModule({
  declarations: [
    GamesComponent,
    GameUpdateComponent,
    GameCreateComponent,
    CategoriesComponent,
    CategoryCreateComponent,
    CategoryUpdateComponent,
    RegisterComponent,
    MarksComponent
  ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        MatButtonModule,
        MatTableModule,
        MatIconModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatSelectModule
    ]
})
export class AdminModule { }
