import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {Game} from "../../../models/game";
import {ShopService} from "../../../services/shop.service";

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  gamesList: Game[] = []
  displayedColumns: string[] = ['position', 'title', 'actions'];
  imgUrl = ''

  constructor(
    private shopService: ShopService
  ) { }

  ngOnInit(): void {
    this.shopService.getGames().subscribe((games) => {
      this.gamesList = games
    })
  }

  remove(id: number) {

  }
}
