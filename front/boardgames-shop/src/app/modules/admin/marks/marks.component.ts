import { Component, OnInit } from '@angular/core';
import {Mark} from "../../../models/mark";
import {ShopService} from "../../../services/shop.service";

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {

  marksList: Mark[];
  displayedColumns: string[] = ['position', 'game-title', 'name', 'mark', 'comment', 'actions'];

  constructor(
    private shopService: ShopService
  ) {
    this.marksList = []
  }

  ngOnInit(): void {
    this.shopService.getMarks()
      .subscribe((res) => {
        this.marksList = res.filter((mark) =>
          mark.moderated == 0
        )
      })
  }

  checkMark(id: number) {

  }

  removeMark(id: number) {

  }

}
