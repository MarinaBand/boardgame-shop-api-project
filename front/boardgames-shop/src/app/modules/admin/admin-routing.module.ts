import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../user/guards/auth.guard";
import {GamesComponent} from "./games/games.component";
import {GameCreateComponent} from "./game-create/game-create.component";
import {GameUpdateComponent} from "./game-update/game-update.component";
import {CategoriesComponent} from "./categories/categories.component";
import {CategoryCreateComponent} from "./category-create/category-create.component";
import {CategoryUpdateComponent} from "./category-update/category-update.component";
import {MarksComponent} from "./marks/marks.component";
import {RegisterComponent} from "./register/register.component";
import {NotFoundComponent} from "../../components/common/not-found/not-found.component";

const routes: Routes = [
  { path: '', redirectTo: '/admin/games', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'games', component: GamesComponent, canActivate: [AuthGuard] },
  { path: 'games/create', component: GameCreateComponent, canActivate: [AuthGuard] },
  { path: 'games/update/:id', component: GameUpdateComponent, canActivate: [AuthGuard] },
  { path: 'categories', component: CategoriesComponent, canActivate: [AuthGuard] },
  { path: 'categories/create', component: CategoryCreateComponent, canActivate: [AuthGuard] },
  { path: 'categories/update/:id', component: CategoryUpdateComponent, canActivate: [AuthGuard] },
  { path: 'marks', component: MarksComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },
  { path: '**', component: NotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
