import { Component, OnInit } from '@angular/core';
import {Error} from "../../../models/error";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AdminService} from "../../../services/admin.service";
import {Category} from "../../../models/category";

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.css']
})
export class CategoryUpdateComponent implements OnInit {

  category: Category;
  errors: Error[];
  form: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adminService: AdminService
  ) {
    this.category = {
      id: 0,
      name: ''
    }
    this.errors = [];
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.adminService.getCategoryById(params['id'])
        .subscribe((res) => {
          if ('error' in res) {
            console.log('ошибка')
          }
          if ('id' in res) {
            this.category = res
          }
        });

    })

  }
  onSave() {
    this.category.name = this.form.value.name
    this.adminService.updateCategory(this.category)
      .subscribe((res) => {
        if (res === 0) {
          this.router.navigate(['admin/categories']);
        }
        if (!Number(res)) {
          this.errors = [];
          this.errors.push(<Error><unknown>res)
        }
      })
  }
}
