import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AdminService} from "../../../services/admin.service";
import {Router} from "@angular/router";
import { Error } from "../../../models/error";

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.css']
})
export class CategoryCreateComponent implements OnInit {

  errors: Error[];
  form: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private adminService: AdminService
  ) {
    this.errors = [];
  }

  ngOnInit(): void {
  }

  onSave() {
    this.adminService.addCategory(this.form.value)
      .subscribe((res) => {
        if (res === 0) {
          this.router.navigate(['admin/categories']);
        }
        if (!Number(res)) {
          this.errors = [];
          this.errors.push(<Error><unknown>res)
        }
      })
  }
}
