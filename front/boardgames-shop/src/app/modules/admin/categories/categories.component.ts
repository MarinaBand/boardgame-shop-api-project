import { Component, OnInit } from '@angular/core';
import {ShopService} from "../../../services/shop.service";
import {environment} from "../../../../environments/environment";
import {Category} from "../../../models/category";
import {AdminService} from "../../../services/admin.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categoriesList: Category[] = []
  displayedColumns: string[] = ['position', 'title', 'actions'];
  imgUrl = ''

  constructor(
    private shopService: ShopService,
    private adminService: AdminService
  ) { }

  ngOnInit(): void {
    this.shopService.getCategories().subscribe((categories) => {
      this.categoriesList = categories
    })
  }

  remove(id: number) {
    if (confirm('Вы действительно хотите удалить категорию?')) {
      this.adminService.deleteCategory(id).subscribe();
      location.reload();
    }

  }

}
