import { Component, OnInit } from '@angular/core';
import {Error} from "../../../models/error";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AdminService} from "../../../services/admin.service";
import {Category} from "../../../models/category";
import {ShopService} from "../../../services/shop.service";
import {Game} from "../../../models/game";

@Component({
  selector: 'app-game-create',
  templateUrl: './game-create.component.html',
  styleUrls: ['./game-create.component.css']
})
export class GameCreateComponent implements OnInit {

  errors: Error[];
  categories: Category[];
  form: FormGroup;

  // form: FormGroup = new FormGroup({
  //   name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zа-я0-9\.:\s]+$/iu)]),
  //   price: new FormControl(null, [Validators.required, Validators.maxLength(5), Validators.pattern(/^[0-9]+$/)]),
  //   playersMin: new FormControl(null, [Validators.required, Validators.maxLength(2), Validators.pattern(/^[0-9]+$/)]),
  //   playersMax: new FormControl(null, [Validators.required, Validators.maxLength(2), Validators.pattern(/^[0-9]+$/)]),
  //   ageRestrictions: new FormControl(null, [Validators.required, Validators.maxLength(2), Validators.pattern(/^[0-9]+$/)]),
  //   inStock: new FormControl(null, [Validators.required, Validators.maxLength(5), Validators.pattern(/^[0-9]+$/)]),
  //   description: new FormControl('', [Validators.required, Validators.maxLength(250)]),
  //   categories: new FormControl('',  Validators.required)
  // });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private adminService: AdminService,
    private shopService: ShopService
  ) {
    this.errors = [];
    this.categories = [];

    this.form = this.formBuilder.group({
      name: '',
      price: 0,
      playersMin: 0,
      playersMax: 0,
      ageRestrictions: 0,
      inStock: 0,
      description: '',
      categories: [''],
      imgFile: ['']
    });
  }
  ngOnInit(): void {
    this.shopService.getCategories().subscribe((res) => {
      this.categories = res;
    })
  }
  onFileSelect(event: Event) {
    const target = event.target as HTMLInputElement;
    const files = target.files as FileList;
    if (files.length > 0) {
      const file = files[0];
      console.log(file);
      // @ts-ignore
      this.form.get('imgFile').setValue(file);
    }
  }

  onSave() {
    const formData = new FormData();
    // @ts-ignore
    formData.append('imgFile', this.form.get('imgFile').value);
    formData.append('name', this.form.value.name);
    formData.append('price', this.form.value.price);
    formData.append('playersMin', this.form.value.playersMin);
    formData.append('playersMax', this.form.value.playersMax);
    formData.append('ageRestrictions', this.form.value.ageRestrictions);
    formData.append('description', this.form.value.description);
    formData.append('inStock', this.form.value.inStock);
    formData.append('categoriesIds', this.form.value.categories.join());

    console.log(this.form.value.categories);
    console.log(this.form.value.categoriesIds);
    this.adminService.addGame(formData)
      .subscribe((res) => {
        if (res === 0) {
          this.router.navigate(['admin/games']);
        }
        if (!Number(res)) {
          this.errors = [];
          // @ts-ignore
          for (let item in res)
          { // @ts-ignore
            this.errors.push(item)
          }
        }
      })
  }
}
