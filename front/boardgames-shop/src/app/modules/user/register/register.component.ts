import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { passwordValidator} from "./validators/password-validator";
import { User } from "../../../models/user";
import { Md5 } from "ts-md5";
import { UserService } from "../../../services/user.service";
import { Error } from "../../../models/error";
import {Router} from "@angular/router";
import {MessengerService} from "../../../services/messenger.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  errors: Error[];
  user: User;
  capcha: string;

  form: FormGroup = new FormGroup({
    account: new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]),
        passwordConfirm: new FormControl(null, Validators.required),
        name: new FormControl('', [Validators.required, Validators.pattern(/^[a-zа-я\s]+$/iu)]),
        phone: new FormControl('', Validators.pattern(/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/)),

      },
      [passwordValidator]
    ),
  });

  constructor(
    private readonly router: Router,
    private message: MessengerService,
    private userService: UserService
  ) {
    this.user = {
      email: '',
      password: '',
      name: '',
      phone: '',
      role: 0
    };
    this.errors = [];
    this.capcha = '';
  }


  ngOnInit(): void {
  }

  resolved(capchaResponse: string) {
    this.capcha = capchaResponse;
    console.log('resolved capcha with reponse: ' + this.capcha);
  }

  submit(): void{
    if (this.form.invalid) {
      return;
    } else {
      const { email, password, name, phone } = this.form.get('account')?.value;
      this.user.email = email;
      this.user.name = name;
      const md5 = new Md5();
      this.user.password = <string>md5.appendStr(password).end();
      this.user.phone = phone;
      this.user.role = 3;
      this.userService.register(this.user).subscribe((res) => {
          if ('error' in res) {
            this.errors.push(res)
            console.log(this.errors)
          }
          if ('token' in res) {
            this.errors = [];
            console.log(res);
            const expDate: Date = new Date(res.tokenTime * 1000);// дата и время истечения токена; токен в секундах * на 1000, получаем милисекунды
            localStorage.setItem('ACCESS_TOKEN', res.token);
            localStorage.setItem('DATE_EXP', expDate.toString());
            localStorage.setItem('UID', res.uid.toString());
            localStorage.setItem('USER_ROLE', res.userRole.toString());
            localStorage.setItem('USER_NAME', res.name.toString());
            this.message.sendIsAuthorized(Number(res.userRole));
            this.router.navigate([`shop/games/`])
          }
        }
      )
      console.log(this.user);
      // console.log(this.md5(password));
      // console.log('Отправляю', email, password, name, phone);
      // console.log(this.form.value);
    }

  }
}
