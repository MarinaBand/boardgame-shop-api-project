import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterComponent} from "./register/register.component";
import {AuthorizeComponent} from "./authorize/authorize.component";
import {NotFoundComponent} from "../../components/common/not-found/not-found.component";

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'auth', component: AuthorizeComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
