import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Md5 } from "ts-md5";
import { UserService } from "../../../services/user.service";
import { Error } from "../../../models/error";
import {Login} from "../../../models/login";
import {Router} from "@angular/router";
import {MessengerService} from "../../../services/messenger.service";

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.css']
})
export class AuthorizeComponent implements OnInit {

  errors: Error[];
  user: Login;
  capcha: string;

  form: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, Validators.required)
  });

  constructor(
    private readonly router: Router,
    private message: MessengerService,
    private userService: UserService
  ) {
    this.user = {
      email: '',
      password: ''
    };
    this.errors = [];
    this.capcha = '';
  }

  ngOnInit(): void {
  }

  resolved(capchaResponse: string) {
    this.capcha = capchaResponse;
    console.log('resolved capcha with response: ' + this.capcha);
  }

  submit(): void{
    if (this.form.invalid) {
      return;
    } else {
      const { email, password } = this.form.value;
      this.user.email = email;
      const md5 = new Md5();
      this.user.password = <string>md5.appendStr(password).end();
      this.userService.signIn(this.user)
        .subscribe((res) => {
          if ('error' in res) {
            this.errors = [];
            this.errors.push(res)
          }
          if ('token' in res) {
            this.errors = [];
            const expDate: Date = new Date(res.tokenTime * 1000); // дата и время истечения токена; токен в секундах * на 1000, получаем милисекунды
            localStorage.setItem('ACCESS_TOKEN', res.token);
            localStorage.setItem('DATE_EXP', expDate.toString());
            localStorage.setItem('UID', res.uid.toString());
            localStorage.setItem('USER_ROLE', res.userRole.toString());
            localStorage.setItem('USER_NAME', res.name.toString());
            this.message.sendIsAuthorized(Number(res.userRole));
            if (Number(res.userRole) == 1 || Number(res.userRole) == 2) {
              this.router.navigate([`/admin/games`]);
            } else {
              this.router.navigate([`/`]);
            }
          }
        });
    }
  }
}
