export interface Mark {
  id?: number;
  value: number;
  comment: string;
  gameId: number;
  gameName?: string;
  userId: number;
  userName?: string;
  moderated: number
}
