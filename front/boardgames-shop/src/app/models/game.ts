export interface Game {
  id?: number;
  name: string;
  price: number;
  playersMin: number;
  playersMax: number;
  ageRestrictions: number;
  description: string;
  imageName: string;
  inStock: number;
  averageMark?: number;
  categories: string;
  categoriesIds: string

}
