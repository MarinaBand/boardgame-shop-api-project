// import { Game } from "./game";

export interface CartItem {
  // id: number;
  gameId: number;
  gameName: string;
  price: number;
  imageName: string;
  quantity: number

  // constructor(quantity: number, game: Game) {
  //   // this.id = id;
  //   this.quantity = quantity;
  //   this.gameId = <number>game.id;
  //   this.gameName = game.name;
  //   this.price = game.price;
  //   this.imageUrl = game.imageUrl;
  // }
}
