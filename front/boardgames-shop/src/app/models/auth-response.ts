export interface AuthResponse {
  uid: number;
  token: string;
  tokenTime: number;
  name: string;
  userRole: number
}
