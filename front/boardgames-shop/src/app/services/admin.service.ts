import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import {Category} from "../models/category";
import {Game} from "../models/game";

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private http: HttpClient
  ) { }

  getCategoryById(id: number): Observable<Category> {
    return this.http.get(environment.mainUrl + `categories/${id}`) as Observable<Category>;
  }
  addCategory(category: Category): Observable<Error | number> {
    return this.http.post(environment.mainUrl + "categories/", category) as Observable<Error>;
  }
  updateCategory({id, ...rest }: Category): Observable<Error | number> {
    return this.http.put(environment.mainUrl + `categories/${id}`, rest) as Observable<Error>;
  }
  deleteCategory(id: number): Observable<Error | number> {
    return this.http.delete(environment.mainUrl + `categories/${id}`) as Observable<Error>;
  }

  getGameById(id: number): Observable<Game> {
    return this.http.get(environment.mainUrl + `games/${id}`) as Observable<Game>;
  }
  addGame(data: FormData): Observable<Error[] | number> {
    return this.http.post(environment.mainUrl + "games/", data) as Observable<Error[]>;
  }
  updateGame({id, ...rest }: Game): Observable<Error | number> {
    return this.http.put(environment.mainUrl + `games/${id}`, rest) as Observable<Error>;
  }
  deleteGame(id: number): Observable<Error | number> {
    return this.http.delete(environment.mainUrl + `games/${id}`) as Observable<Error>;
  }
}
