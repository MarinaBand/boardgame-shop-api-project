import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "../models/user";
import { Error } from "../models/error";
import { Login } from "../models/login";
import { AuthResponse } from "../models/auth-response";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  register(user: User): Observable<Error | AuthResponse> {
    return this.http.post(environment.mainUrl + "reg/", user) as Observable<AuthResponse>;
  }

  signIn(user: Login): Observable<Error | AuthResponse> {
    return this.http.post(environment.mainUrl + "auth/", user) as Observable<AuthResponse>;
  }

  logOut(userId:number):Observable<string | number> {
    return this.http.delete(environment.mainUrl + `auth/${userId}`) as Observable<string>;
  }

  isAuthorized(): number {
    const dateExp: string = <string>localStorage.getItem('DATE_EXP');
    if (!dateExp) {
      return 0;
    }
    if (new Date() > new Date(dateExp)) {
      this.logOut(Number(localStorage.getItem('UID')));
      localStorage.removeItem('DATE_EXP');
      localStorage.removeItem('ACCESS_TOKEN');
      localStorage.removeItem('USER_NAME');
      localStorage.removeItem('USER_ROLE');
      localStorage.removeItem('UID');
      return 0;
    }
    return Number(localStorage.getItem('USER_ROLE'));
  }
}
