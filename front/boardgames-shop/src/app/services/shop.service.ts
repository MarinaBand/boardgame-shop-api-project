import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Game } from "../models/game";
import { Category } from "../models/category";
import {Observable} from "rxjs";
import {Mark} from "../models/mark";

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(
    private http: HttpClient
  ) { }

  getGames(): Observable<Game[]> {
    return this.http.get(environment.mainUrl + "games/") as Observable<Game[]>;
  }
  getGameById(id: number): Observable<Game> {
    return this.http.get(environment.mainUrl + `games/${id}`) as Observable<Game>;
  }
  getCategories(): Observable<Category[]> {
    return this.http.get(environment.mainUrl + "categories/") as Observable<Category[]>;
  }
  getMarks(): Observable<Mark[]> {
    return this.http.get(environment.mainUrl + "marks/") as Observable<Mark[]>;
  }
  getMarkById(id: number): Observable<Mark> {
    return this.http.get(environment.mainUrl + `marks/${id}`) as Observable<Mark>;
  }
  addMark(mark: Mark): Observable<Error | number> {
    return this.http.post(environment.mainUrl + `marks/`, mark) as Observable<Error>;
  }
}
