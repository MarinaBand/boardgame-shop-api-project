import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { Game } from "../models/game";

@Injectable({
  providedIn: 'root'
})
export class MessengerService {

  gameSubject = new Subject<Game>()
  quantitySubject = new Subject<number>()
  authorizedSubject = new Subject<number>()

  constructor(
  ) { }

  sendGameToCart(game: Game) {
    console.log(game);
    this.gameSubject.next(game)
  }

  getGameToCart() {
    return this.gameSubject.asObservable()
  }

  sendQtyToHeader(num: number) {
    this.quantitySubject.next(num)
  }

  getQtyToHeader() {
    return this.quantitySubject.asObservable()
  }

  sendIsAuthorized(num: number) {
    this.authorizedSubject.next(num)
  }

  getIsAuthorized() {
    return this.authorizedSubject.asObservable()
  }

}
